# class Board
#   attr_reader :grid
#
#   def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
#     @grid = grid
#   end
#
#   def place_mark(pos, mark)
#     @grid[pos[0]][pos[1]] = mark
#   end
#
#   def empty?(pos)
#     return true if grid[pos[0]][pos[1]] == nil
#     false
#   end
#
#   def winner
#     #rows
#     return :X if grid[0].all? {|el| el == :X}
#     return :X if grid[1].all? {|el| el == :X}
#     return :X if grid[2].all? {|el| el == :X}
#     return :O if grid[0].all? {|el| el == :O}
#     return :O if grid[1].all? {|el| el == :O}
#     return :O if grid[2].all? {|el| el == :O}
#     #columns
#     return :X if grid.each_index.all? {|i| grid[i][0] == :X}
#     return :X if grid.each_index.all? {|i| grid[i][1] == :X}
#     return :X if grid.each_index.all? {|i| grid[i][2] == :X}
#     return :O if grid.each_index.all? {|i| grid[i][0] == :O}
#     return :O if grid.each_index.all? {|i| grid[i][1] == :O}
#     return :O if grid.each_index.all? {|i| grid[i][2] == :O}
#     #diagonals
#     return :X if grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :X
#     return :X if grid[0][0] == :X && grid[1][1] == :X && grid[2][2] == :X
#     return :O if grid[0][2] == :O && grid[1][1] == :O && grid[2][0] == :O
#     return :O if grid[0][0] == :O && grid[1][1] == :O && grid[2][2] == :O
#   end
#
#   def over?
#     return true if winner
#     #return false if grid == nil
#     return true if grid.flatten.none? {|el| el == nil} && !winner
#   end
# end








class Board
  attr_accessor :grid

  def initialize(grid = Array.new(3) {Array.new(3)})
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil
  end

  def winner
    #rows
    return :X if @grid[0].all? {|square| square == :X}
    return :X if @grid[1].all? {|square| square == :X}
    return :X if @grid[2].all? {|square| square == :X}
    #diagonals
    return :X if grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :X
    return :X if grid[0][0] == :X && grid[1][1] == :X && grid[2][2] == :X
    #columns
    return :O if @grid.all? {|el| el[0] == :O}
    return :O if @grid.all? {|el| el[1] == :O}
    return :O if @grid.all? {|el| el[2] == :O}
  end

  def over?
    # return false if @grid.flatten.all? {|el| el == nil}
    return true if @grid.flatten.none? {|el| el == nil} || winner
  end
end
