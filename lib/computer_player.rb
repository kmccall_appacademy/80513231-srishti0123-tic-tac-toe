# class ComputerPlayer
#   attr_accessor :name, :board, :mark
#
#   def initialize(name)
#     @name = name
#     @mark = :O
#   end
#
#   def display(board)
#     @board = board
#   end
#
#   def get_move
#     #creates array of nil positions
#     empty_slots = []
#     board.grid.each_index do |row|
#       board.grid[row].each_index do |col|
#         if board.grid[row][col] == nil
#           empty_slots << [row, col]
#         end
#       end
#     end
#     #searches for and returns winner if found
#     empty_slots.each do |pos|
#       board.grid[pos[0]][pos[1]] = mark
#       if board.winner
#         return pos
#       else
#         board.grid[pos[0]][pos[1]] = nil
#       end
#     end
#     #otherwise, random move
#     empty_slots.sample
#   end
#
# end




























class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    empty_slots = []
    (0..2).each do |row|
      (0..2).each do |col|
        empty_slots << [row, col] if board.grid[row][col].nil?
      end
    end

    #searches for and returns winner if found
    empty_slots.each do |pos|
      board.grid[pos[0]][pos[1]] = mark
      if board.winner
        return pos
      else
        board.grid[pos[0]][pos[1]] = nil
      end
    end
    #otherwise, random move
    empty_slots.sample
  end


  #   moves.each do |move|
  #     return move if wins?(move)
  #   end
  #
  #   moves.sample
  # end
  #
  # def wins?(move)
  #   board.grid[move[0]][move[1]] = mark
  #   if board.winner == mark
  #     board.grid[move[0]][move[1]] = nil
  #     true
  #   else
  #     board.grid[move[0]][move[1]] = nil
  #     false
  #   end
  # end




end
